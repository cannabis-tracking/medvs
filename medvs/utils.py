"""utils module."""

import string
from datetime import date
from datetime import timedelta

from medvs import templates

""" Functions """


def getDateFormat(Separator):
    """Gets a date format as a string.
    Avoids long line obfuscation.
    """
    return templates.Date_Format.format(Separator=Separator)


def getDate(Separator='/'):
    """Get mm/dd/yyyy formatted date string."""
    todays_date = date.strftime(date.today(), getDateFormat(Separator))

    return todays_date


def getWednesdaysDate(Separator='/'):
    """Gets the previous Wednesdays date using getDateFormat for the format."""
    today = date.today()
    offset = (today.weekday() - 2) % 7
    last_wednesday = today - timedelta(days=offset)

    # Formatting
    wednesday = date.strftime(last_wednesday, getDateFormat(Separator))

    return wednesday


def getWordFromNumber(number):
    """Gets a spelled string from an individual digit."""
    isNumeric = str(number).isnumeric()
    if not isNumeric:
        raise TypeError

    number = int(number)
    isOutOfRange = (number > 9 or number < 0)
    if isOutOfRange:
        raise IndexError

    return templates.Spelled_Digits[number]


def getWordFromAbbreviation(abbreviation):
    """Gets a fully-spelled version of an abbreviation.
    If abbreviation is not in templates.Spelled_Words a LookupError is
    raised.
    """
    abbreviation = abbreviation.upper()
    if abbreviation not in templates.Spelled_Words:
        raise LookupError

    return templates.Spelled_Words[abbreviation]


def getSpelledStrain(Strain):
    """Gets a fully-spelled version of a strain with number and
    abbreviations.
    """
    spelled_strain = []

    for word in Strain.split(' '):
        word = word.strip(string.punctuation)
        if word.isnumeric():
            if int(word) in templates.Spelled_Digits:
                spelled_strain.append(getWordFromNumber(word))

        elif word.upper() in templates.Spelled_Words:
            spelled_strain.append(getWordFromAbbreviation(word))

        else:
            spelled_strain.append(word)

    return " ".join(spelled_strain)


def getStrainCode(Strain):
    """Gets the first two characters of each word from a fully spelled
    strain name."""
    strain_code = str()

    spelled_strain = getSpelledStrain(Strain)

    for word in spelled_strain.split(' '):
        strain_code += word[:2]

    return strain_code.lower()


def getStrainCodes(Strains):
    """Gets a list of strain codes from a list of strains."""
    strain_codes = []

    for strain in Strains:
        strain_codes.append(getStrainCode(strain))

    return strain_codes


def convertToStripped(in_sheetlist):
    """Converts in_sheetlist by applying .strip() to all values"""
    for row in in_sheetlist:
        for cell_value in row:
            if isinstance(cell_value, str):
                cell_value = cell_value.strip()


def convertNoneToString(in_sheetlist):
    """Converts in_sheetlist None values to empty string."""
    for row in in_sheetlist:
        for cell_value in row:
            if cell_value is None:
                cell_value = str()


def getRoomGroups(in_row):
    """Takes a row of text from getSheetlist.
    Returns a list containing the letter and number of the room.
    If match is not found,
    Returns a list containing an empty string and then a 0.
    """

    regex_match = (templates.Room_Pattern
                   .search(str(in_row[templates.Room_Index])))

    if regex_match is not None:
        return [regex_match.group(1), int(regex_match.group(2))]

    else:
        return ["", 0]


def f_getRoomLetter(in_room_split): return getRoomGroups(in_room_split)[0]


"""Takes a list containing the letter and then number of a room,
Returns the letter.
"""


def f_getRoomNumber(in_room_split): return getRoomGroups(in_room_split)[1]


"""Takes a list containing the letter and then number of a room,
Returns the number.
"""

""" End Functions """
