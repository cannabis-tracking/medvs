################################### Todo ######################################
### Issues ###
# FIX CONSTANTS (it should at least run...)
# Erases sheet when working on an open workbook
# Items left in column C are being duplicated in file due to not being erased
### Add ###
# Excel, Path, and Template modules
# Docstrings
# metrc logging
## README ##
# How-To-Use
## Variables ##
# CONSTANTS.Name
## Functionality ##
# getSet() # Room and Strain sets to compare if any are unaccounted for
# Detect if harvest' is different case or misspelled path
# Get arguments from sys.argv[] && validate
# Generic countErrors that takes a lambda that returns a bool (for each error)
### Change ###
# Clean up getSheetlist
## Use-Of ##
# import *
# Debug log save location
# Fix info and debug use
# Use traceback, come-on, man!
## Style ##
# Decapitalize any non-constants
# Return None on void functions / fail
# Comments way off
# Newline between if and else
# Comments ending code blocks
##### If ####
#### MOVE Sections to Separate Files ####
#### Create new sheet for validated ####
### Add ###
# Merged cells above data with borders
# Worksheet.active_cell = "A3"
##### Excel Validation Translation #####
## General ##
# Strip Cells
# Pull cell ranges from named ranges
## Weights ##
# isEven
# is greater than 1400
# is numeric
# RFIDs
# regex 1A4000(21266EDF4000) [lic: 403R-00020]
# isAlphaNum
# len == 24
# Check for duplicate values
## if rooms are "Not Found" ##
## if strain are "Not Found" ##
#################################### Todo #####################################
# ADD adjacent change function for rooms and strains
# 
# Install autopep8 just in case
