"""core module."""

try:
    from medvs.core import *
    from medvs import all_imports

except ImportError as current_exception:
    print(current_exception)

    import sys
    import os
    sys.path.append(os.path.join(os.path.dirname(sys.argv[0]),
                                 ".."))
    from medvs.core import *
    from medvs import all_imports

# from medvs import logger
import logging
logging.disable(logging.CRITICAL)  # update logger

""" Constants """
# METRC Constants
CO_State_Code = "020"
""" End Constants """
