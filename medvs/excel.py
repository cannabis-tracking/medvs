"""The medvs.medvs.excel module is for handling all excel-based files
"""
import sys
import time

import openpyxl
from openpyxl.utils import get_column_letter

from medvs import templates

# Move these constants to tgs
# Sheetnames
l_Weights = "Weights"
l_Report = "Report"

# Column Ranges
RFID = "A3:A1000"
WEIGHT = "B3:B1000"
STRAIN = "C3:C1000"
ROOM = "D3:D1000"

# Sheet Ranges
Weights_Range = "$A$3:$D$1000"


""" Constants """
# File Extensions
Extension = ".xlsx"


""" Functions """


def getSheetlist(Filename, Sheetname):
    # Takes and openpyxl workbook and sheetname
    # Returns a worksheet as a list
    workbook = openpyxl.load_workbook(Filename, read_only=True, data_only=True)
    Sheet = workbook[Sheetname]

    raw_rows = list()  # create a list at function scope
    out_filtered_rows = list()  # create a sorted list at function scope

    for row in Sheet.iter_rows(min_row=templates.Start_Row,
                               max_row=templates.End_Row,
                               max_col=templates.End_Column,
                               values_only=True):
        # logger.debug("Adding raw row %s" % (str(row)))
        raw_rows.append(row)

    # Remove None values from the list
    for sublist in raw_rows:
        # Remove None values from sublist
        tmp_filtered_rows = [row for row in sublist if row is not None]

        # Check if any values are left
        if (len(tmp_filtered_rows) > 0) and ((sublist[0] is not None) and
                                             (sublist[1] is not None)):
            # logger.info("Adding filtered row: %s" % (str(sublist)))
            out_filtered_rows.append(sublist)  # append if list is left

    return out_filtered_rows


def saveListAs(in_workbook, in_Sheetname, in_rows, in_Validated_File_Path):
    """Takes a Workbook, a Sheetname, and a 2D list to insert.
    Saves 2D as an Excel Spreadsheet.
    Returns None if succesful, if fail, throws exception and exits.
    """
    Sheet = in_workbook[in_Sheetname]
    """
    logger.info("Getting column styles...")
    Column_Styles = [copy(Sheet[RFID[:2]].style),
                     copy(Sheet[WEIGHT[:2]].style),
                     copy(Sheet[CORRECTED[:2]].style),
                     copy(Sheet[STRAIN[:2]].style),
                     copy(Sheet[ROOM[:2]].style)]
    """
    # Clears column C
    # Sheet.delete_cols(Corrected_Index + 1)

    # Set each item to corresponding cell
    for row_number, row in enumerate(in_rows, start=templates.Start_Row):
        for column_number, cell_value in enumerate(row, start=1):
            cell_coordinate = get_column_letter(column_number)
            cell_coordinate += str(row_number)

            """
            cell = Sheet[cell_coordinate]  # for below comment?
            # Get copies of cell styles
            if cell.has_style:
                cell_font = copy(cell.font)
                cell_border = copy(cell.border)
                cell_fill = copy(cell.fill)
                cell_number_format = copy(cell.number_format)
                cell_protection = copy(cell.protection)
                cell_alignment = copy(cell.alignment)
            """

            # skip empty values
            if (cell_value == "") or (cell_value is None):
                continue

            # convert value to proper data type
            if column_number == 2:
                Sheet[cell_coordinate].value = float(cell_value)

            else:
                Sheet[cell_coordinate].value = cell_value

            """
            # Set cell styles
            if cell.has_style:
                cell.font = cell_font
                cell.border = cell_border
                cell.fill = cell_fill
                cell.number_format = cell_number_format
                cell.protection = cell_protection
                cell.alignment = cell_alignment
            """

            # logger.info("Changed %s to %s"
            #             % (cell_coordinate, str(cell.value)))
        # End for
    # End for

    # Clear cells below data
    data_end = len(in_rows) + templates.Start_Row

    # logger.info("Clearing original worksheet from row %s to row %s"
    #             % (data_end, templates.End_Row))
    Sheet.delete_rows(data_end, templates.End_Row - data_end)

    # Save Validated Workbook

    # logger.info("Saving validated spreadsheet...")

    try:
        in_workbook.save(in_Validated_File_Path)

    except OSError as current_exception:
        print(current_exception)
        # logger.critical(current_exception)
        # logger.critical(str(traceback.format_exc()))
        # logger.critical("Unable to save file!")
        # logger.critical("The filename attempted was %s"
        #                 % (in_Validated_File_Path))
        # logger.critical("Exiting program in 20 seconds...\n")
        time.sleep(20)
        sys.exit(-1)

    # logger.info("Saved at %s" % (in_Validated_File_Path))

    return None


""" End Functions """
