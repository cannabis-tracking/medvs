"""Docstring."""

import re

""" Constants """
Length = 24
Pattern = re.compile("(1A400021266EDF4)(\\d{9})")
""" End Constants """

""" Functions """


def isValidLength(RFID):
    """Docstring."""
    if len(RFID) == Length:
        return True
    else:
        return False


def isValidPattern(RFID):
    """Docstring."""
    match = Pattern.search(RFID)
    if match is not None:
        return True
    else:
        return False


def isValid(RFID):
    """Docstring."""
    if isValidLength(RFID) and isValidPattern(RFID):
        return True
    else:
        return False


def getValidated(RFID):
    """Docstring."""
    if isValid(RFID):
        if RFID.isupper():
            return RFID
        else:
            return RFID.upper()
    else:
        raise Exception


""" End Functions """
