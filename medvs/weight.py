"""Docstring."""

""" Constants """
Warn_Limit = 1400
Critical_Limit = 3000
""" End Constants """

""" Functions """


def isEven(Weight):
    """Docstring."""
    if Weight % 2 == 0:
        return True
    else:
        return False


def isValid(Weight):
    """Docstring."""
    if isEven(Weight) and Weight < Warn_Limit:
        return True
    else:
        return False


def getValidated(Weight):
    """Docstring."""
    if isValid(Weight):
        return float(Weight)
    else:
        if not isEven(Weight):
            Weight -= 1.00

        if Weight < Critical_Limit:
            return float(Weight)
        else:
            raise Exception


""" End Functions """
