@echo off

:: This batch file downloads an update for medvs from GitLab
:: Last Updated: June 24, 2019

title Update-Scripts

echo Updating...
echo.

:: Requires Git-Bash and Later Python3
echo This updater REQUIRES git!
echo The '.py' file REQUIRES Python3

:: Checking for Internet

echo Checking for Internet connection...
ping www.google.nl -n 1 -w 1000

if %errorlevel%==1 (
:: If connection fails...
echo.
echo No Internet connection...
echo Cannot update without Internet...
echo Exiting the updater...
echo.
pause
exit
)

:: If connection is successful...
echo Internet connection appears to be working...

:: Set Variables and Delete Current Files

set "URL=https://gitlab.com/jcarpinelli/medvs.git/"

echo.
echo If you need to keep any files inside the medvs folder,
echo get them now...
echo.
echo The updater will delete the entire medvs folder before replacing it...

pause

echo.
echo Deleting current files in order to update...
if exist .git recycle /s /q .git

if exist med-vs rmdir /s /q med-vs

:: Update medvs

echo Adding medvs repo
git clone %URL%
echo.

echo This script does not check for success...
echo If there are no errors, medvs should be updated...
echo Update Complete!
echo.

:: End Script
timeout \t 5
