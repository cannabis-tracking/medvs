#! /usr/bin/env python3
# -*- coding: utf-8 -*-


# import sys
# from shutil import rmtree
import os

from setuptools import find_packages, setup  # , Command


""" Meta-Data """
NAME = "medvs"
VERSION = "0.0.1a1"
DESCRIPTION = (
    "Format and validate tables containing data ultimately intended for METRC."
    )
LONG_DESCRIPTION = ()
LONG_DESCRIPTION_CONTENT_TYPE = "text/markdown"
URL = "https://github.com/jcarpinelli/medvs"
AUTHOR = "Joseph Carpinelli"
AUTHOR_EMAIL = "jcarpinelli@acm.org"
LICENSE = "GPLv3"
CLASSIFIERS = [
        # Trove classifiers:
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Customer Service",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Operating System :: POSIX :: Linux",
        "Operating System :: Microsoft :: Windows",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Topic :: Database"
]
KEYWORDS = "csv excel med metrc plants tracker"
PROJECT_URLS = {
                # Project URL Examples
                # 'Documentation': 'https://',
                # 'Funding': 'https://',
                # 'Say Thanks!': 'http://bitbucket.org/openpyxl/openpyxl/',
                # 'Source': 'https://',
                # 'Tracker': 'https://',
}
REQUIRED = [
            # "xlrd",
            "openpyxl>=2.6.2"
]
REQUIRES_PYTHON = ">=3.6.0"
""" End Meta-Data """


""" LICENSE, README, and VERSION """
with open("LICENSE", encoding="utf-8") as f:
    license = f.read()

Working_Directory = os.path.abspath(os.path.dirname(__file__))

# Import README to use as the long_description (if in MANIFEST.in)
try:
    with open(os.path.join(Working_Directory, "README.md"),
              encoding="utf-8") as f:
        long_description = '\n' + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    project_slug = NAME.lower().replace("-", "_").replace(" ", "_")
    with open(os.path.join(Working_Directory, project_slug, "__version__.py")
              ) as f:
        exec(f.read(), about)
else:
    about["__version__"] = VERSION
""" End LICENSE, README, and VERSION """


setup(
    name=NAME,
    version=about["__version__"],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type=LONG_DESCRIPTION_CONTENT_TYPE,
    url=URL,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    license=LICENSE,
    classifiers=CLASSIFIERS,
    keywords=KEYWORDS,
    project_urls=PROJECT_URLS,
    packages=find_packages(exclude=["build", "data", "docs", "contrib",
                                    "tests*"]),
    # If your project contains any single-file Python modules that aren’t
    # part of a package, set py_modules to a list of the names of the modules
    # (minus the .py extension) in order to make setuptools aware of them:
    # py_modules=["mypackage"],
    install_requires=REQUIRED,
    python_requires=REQUIRES_PYTHON,
    include_package_data=True,
)
