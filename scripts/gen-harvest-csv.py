#! /usr/bin/env python3

# gen-harvest-csv.py
# Generates a CSV for upload to METRC
# Joseph Carpinelli
# June 21st, 2019


# Get a Veg_List
# Add getStrainCode
# Add new date format

# Move excel.py constants to templates
# consider moving templates to constants.etc


import os
import csv

try:
    import medvs  # if package is installed

except ImportError as current_exception:
    print(current_exception)
    print("medvs is not installed")
    print("Running as portable")

    import sys
    sys.path.append(os.path.join(os.path.dirname(sys.argv[0]),
                                 ".."))
    import medvs

# Read Lists
input_table = [[]]
tags = []
weights = []
strains = []

# Modified Lists
strain_codes = []
tag_rooms_flagged = []

# Output Variables
csv_rows = []


# Variables
date = medvs.utils.getDate('.')
wednesdays_date = medvs.utils.getWednesdaysDate()

# Path
validated_file = input(
    "Drag the harvest file into this window and press enter: ")
validated_file = validated_file.strip("'\" ")
if not os.path.exists(validated_file):
    basename = os.path.basename(validated_file)
    validated_file = os.path.join(medvs.path.Working_Directory, basename)
    if not medvs.path.exists(validated_file):
        validated_file = os.path.join(medvs.path.Output_Directory, basename)
        if not medvs.path.exists(validated_file):
            raise FileNotFoundError

cycle = input("Enter the harvest cycle: ")

harvest_csv = os.path.join(medvs.path.Output_Directory,
                           "{date} Harvest File.csv".format(date=date))
error_csv = os.path.join(medvs.path.Output_Directory,
                         "{date} Error File.csv".format(date=date))


# Get validated data
if validated_file.endswith(medvs.path.CSV_Extension):
    with open(validated_file, 'r') as csv_in:
        csv_reader = csv.reader(csv_in)
        input_table = list(csv_reader)

elif validated_file.endswith(medvs.path.XLSX_Extension):
    input_table = medvs.excel.getSheetlist(validated_file,
                                           medvs.excel.l_Weights)


# Build Data Table
for row in input_table:
    if ("Harvest" in row[0]) or ("RFID" in row[0]):
        continue

    if row[0] in medvs.templates.Blanks:
        continue

    tags.append(row[0])
    weights.append(row[1])
    strains.append(row[2])

    # if row[3] is in veg_list:
    #   tag_rooms_flagged.append([row[0], row[3]])

    if row[3] is medvs.templates.Not_Found:
        tag_rooms_flagged.append([row[0], row[3]])

# Verify List Size
assert len(tags) == len(weights) == len(strains)

# Build Strain Codes
strain_codes = medvs.utils.getStrainCodes(strains)

# Build CSV Table
for index, strain in enumerate(strains):
    csv_row = []

    csv_row.append(tags[index])
    csv_row.append(weights[index])
    csv_row.append(medvs.templates.Units)
    csv_row.append(medvs.templates.Cycle_Format.format(Cycle=cycle))
    csv_row.append(medvs.templates.Batchname_Format
                   .format(date=wednesdays_date,
                           State_Code=medvs.CO_State_Code,
                           strain_code=strain_codes[index]))
    csv_row.append(None)
    csv_row.append(wednesdays_date)

    csv_rows.append(csv_row)

with open(harvest_csv, 'w', newline='') as harvest_out:
    csv_writer = csv.writer(harvest_out)
    csv_writer.writerows([row for row in csv_rows])

if len(tag_rooms_flagged) > 0:
    with open(error_csv, 'w', newline='') as error_out:
        csv_writer = csv.writer(error_out)
        csv_writer.writerows([row for row in tag_rooms_flagged])

print("Files saved in 'Output Files'")
